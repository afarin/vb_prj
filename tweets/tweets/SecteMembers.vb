﻿Public Structure SecteMembers

    Private IdSecteMembers As Integer
    Private nomDeFollowings As Integer
    Private nomDeFollowers As Integer
    Private rapportFollowFallowers As Double
    Private nbTotDeTweets As Integer
    Private url As Double
    Private arobase As Double

    Public Sub New(Id As Integer, nomDeFollowings As Integer, nomDeFollowers As Integer, rapportFollowFallowers As Double,
                   nbTotDeTweets As Integer, url As Double, arobase As Double)
        Me.IdSecteMembers = Id
        Me.nomDeFollowings = nomDeFollowings
        Me.nomDeFollowers = nomDeFollowers
        Me.rapportFollowFallowers = rapportFollowFallowers
        Me.nbTotDeTweets = nbTotDeTweets
        Me.url = url
        Me.arobase = arobase

    End Sub

    Public Property ProprieteIdSecteMembers As Integer
        Get
            Return Me.IdSecteMembers
        End Get
        Set(value As Integer)
            Me.IdSecteMembers = value
        End Set
    End Property

    Public Property ProprietenbFollower As Integer
        Get
            Return Me.nomDeFollowers
        End Get
        Set(value As Integer)
            Me.nomDeFollowers = value
        End Set
    End Property

    Public Property ProprietenbFollowing As Integer
        Get
            Return Me.nomDeFollowings
        End Get
        Set(value As Integer)
            Me.nomDeFollowings = value
        End Set
    End Property

    Public Property ProprietenbTweet As Integer
        Get
            Return Me.nbTotDeTweets
        End Get
        Set(value As Integer)
            Me.nbTotDeTweets = value
        End Set
    End Property

    Public Property ProprieUrl As Double
        Get
            Return Me.url
        End Get
        Set(value As Double)
            Me.url = value
        End Set
    End Property

    Public Property ProprieArobase As Double
        Get
            Return Me.arobase
        End Get
        Set(value As Double)
            Me.arobase = value
        End Set
    End Property
    Public Property ProprieteRatiofollowsurfollower As Double
        Get
            Return Math.Round(Me.nomDeFollowings / Me.nomDeFollowers, 2)
        End Get
        Set(value As Double)
            Me.rapportFollowFallowers = value
        End Set
    End Property
End Structure
