﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.btnUserFile = New System.Windows.Forms.Button()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.txbUserFile = New System.Windows.Forms.TextBox()
        Me.btnUsersTwwet = New System.Windows.Forms.Button()
        Me.txbUserTweet = New System.Windows.Forms.TextBox()
        Me.OpenFileDialog2 = New System.Windows.Forms.OpenFileDialog()
        Me.btnSauvegarder = New System.Windows.Forms.Button()
        Me.txbSauvegarder = New System.Windows.Forms.TextBox()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.btnExec = New System.Windows.Forms.Button()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnOrd = New System.Windows.Forms.Button()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnUserFile
        '
        Me.btnUserFile.Location = New System.Drawing.Point(26, 20)
        Me.btnUserFile.Name = "btnUserFile"
        Me.btnUserFile.Size = New System.Drawing.Size(172, 50)
        Me.btnUserFile.TabIndex = 0
        Me.btnUserFile.Text = "Selsect user_File"
        Me.btnUserFile.UseVisualStyleBackColor = True
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'txbUserFile
        '
        Me.txbUserFile.Location = New System.Drawing.Point(251, 32)
        Me.txbUserFile.Name = "txbUserFile"
        Me.txbUserFile.Size = New System.Drawing.Size(798, 26)
        Me.txbUserFile.TabIndex = 1
        '
        'btnUsersTwwet
        '
        Me.btnUsersTwwet.Location = New System.Drawing.Point(26, 114)
        Me.btnUsersTwwet.Name = "btnUsersTwwet"
        Me.btnUsersTwwet.Size = New System.Drawing.Size(172, 50)
        Me.btnUsersTwwet.TabIndex = 2
        Me.btnUsersTwwet.Text = "Select users_tweets"
        Me.btnUsersTwwet.UseVisualStyleBackColor = True
        '
        'txbUserTweet
        '
        Me.txbUserTweet.Location = New System.Drawing.Point(260, 114)
        Me.txbUserTweet.Name = "txbUserTweet"
        Me.txbUserTweet.Size = New System.Drawing.Size(798, 26)
        Me.txbUserTweet.TabIndex = 3
        '
        'OpenFileDialog2
        '
        Me.OpenFileDialog2.FileName = "OpenFileDialog2"
        '
        'btnSauvegarder
        '
        Me.btnSauvegarder.Location = New System.Drawing.Point(26, 198)
        Me.btnSauvegarder.Name = "btnSauvegarder"
        Me.btnSauvegarder.Size = New System.Drawing.Size(172, 51)
        Me.btnSauvegarder.TabIndex = 4
        Me.btnSauvegarder.Text = "Sauvegarder"
        Me.btnSauvegarder.UseVisualStyleBackColor = True
        '
        'txbSauvegarder
        '
        Me.txbSauvegarder.Location = New System.Drawing.Point(274, 223)
        Me.txbSauvegarder.Name = "txbSauvegarder"
        Me.txbSauvegarder.Size = New System.Drawing.Size(798, 26)
        Me.txbSauvegarder.TabIndex = 5
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column2, Me.Column3, Me.Column4, Me.Column5, Me.Column6, Me.Column7})
        Me.DataGridView1.Location = New System.Drawing.Point(128, 345)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.RowTemplate.Height = 28
        Me.DataGridView1.Size = New System.Drawing.Size(993, 306)
        Me.DataGridView1.TabIndex = 6
        '
        'btnExec
        '
        Me.btnExec.Location = New System.Drawing.Point(26, 271)
        Me.btnExec.Name = "btnExec"
        Me.btnExec.Size = New System.Drawing.Size(172, 54)
        Me.btnExec.TabIndex = 7
        Me.btnExec.Text = "Executer"
        Me.btnExec.UseVisualStyleBackColor = True
        '
        'Column1
        '
        Me.Column1.HeaderText = "Id"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        '
        'Column2
        '
        Me.Column2.HeaderText = "N.followings"
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        '
        'Column3
        '
        Me.Column3.HeaderText = "N.followers"
        Me.Column3.Name = "Column3"
        Me.Column3.ReadOnly = True
        '
        'Column4
        '
        Me.Column4.HeaderText = "wings/wers"
        Me.Column4.Name = "Column4"
        Me.Column4.ReadOnly = True
        '
        'Column5
        '
        Me.Column5.HeaderText = "N.total.tweets"
        Me.Column5.Name = "Column5"
        Me.Column5.ReadOnly = True
        '
        'Column6
        '
        Me.Column6.HeaderText = "R.URL / tweets" & Global.Microsoft.VisualBasic.ChrW(9)
        Me.Column6.Name = "Column6"
        Me.Column6.ReadOnly = True
        '
        'Column7
        '
        Me.Column7.HeaderText = "R.@ / tweets"
        Me.Column7.Name = "Column7"
        Me.Column7.ReadOnly = True
        '
        'btnOrd
        '
        Me.btnOrd.Location = New System.Drawing.Point(458, 276)
        Me.btnOrd.Name = "btnOrd"
        Me.btnOrd.Size = New System.Drawing.Size(180, 45)
        Me.btnOrd.TabIndex = 8
        Me.btnOrd.Text = "Ordonner"
        Me.btnOrd.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1259, 743)
        Me.Controls.Add(Me.btnOrd)
        Me.Controls.Add(Me.btnExec)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.txbSauvegarder)
        Me.Controls.Add(Me.btnSauvegarder)
        Me.Controls.Add(Me.txbUserTweet)
        Me.Controls.Add(Me.btnUsersTwwet)
        Me.Controls.Add(Me.txbUserFile)
        Me.Controls.Add(Me.btnUserFile)
        Me.Name = "Form1"
        Me.Text = "Form1"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnUserFile As Button
    Friend WithEvents OpenFileDialog1 As OpenFileDialog
    Friend WithEvents txbUserFile As TextBox
    Friend WithEvents btnUsersTwwet As Button
    Friend WithEvents txbUserTweet As TextBox
    Friend WithEvents OpenFileDialog2 As OpenFileDialog
    Friend WithEvents btnSauvegarder As Button
    Friend WithEvents txbSauvegarder As TextBox
    Friend WithEvents SaveFileDialog1 As SaveFileDialog
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents btnExec As Button
    Friend WithEvents Column1 As DataGridViewTextBoxColumn
    Friend WithEvents Column2 As DataGridViewTextBoxColumn
    Friend WithEvents Column3 As DataGridViewTextBoxColumn
    Friend WithEvents Column4 As DataGridViewTextBoxColumn
    Friend WithEvents Column5 As DataGridViewTextBoxColumn
    Friend WithEvents Column6 As DataGridViewTextBoxColumn
    Friend WithEvents Column7 As DataGridViewTextBoxColumn
    Friend WithEvents btnOrd As Button
End Class
