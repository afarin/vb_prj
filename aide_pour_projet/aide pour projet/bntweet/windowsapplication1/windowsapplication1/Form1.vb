﻿Imports System.IO
Public Structure info
    Public id As Int16
    Public nbTweet As Int16
End Structure
Public Class Form1

    Public list As ArrayList
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click

        Dim temp As info = New info()

        'réccupération de tous les id'S du fichier id.txt
        list = New ArrayList()
        Dim searcher As OpenFileDialog = New OpenFileDialog()
        searcher.ShowDialog()
        Dim lecteur As StreamReader = New StreamReader(searcher.FileName)

        While (Not lecteur.EndOfStream)

            temp.id = CInt(lecteur.ReadLine())
            list.Add(temp)
        End While

        lecteur.Close()

        'nb de tweet par id
        searcher.ShowDialog()
        lecteur = New StreamReader(searcher.FileName)

        Dim pointeur As Int16

        While (Not lecteur.EndOfStream)

            If (CType(list(pointeur), info).id = CInt(lecteur.ReadLine())) Then
                temp = CType(list(pointeur), info)
                temp.nbTweet += 1
                list(pointeur) = temp
            Else
                pointeur += 1
            End If
        End While
        lecteur.Close()
    End Sub
End Class
